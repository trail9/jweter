/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import javax.swing.JOptionPane
import org.xml.sax.SAXParseException
import javax.swing.SwingWorker

/**
 * SwingWorker for reading/parsing JMeter result (.jtl) files.
 */
class JtlFileReaderWorker extends SwingWorker {

    def layout
    JMeterResults resultsProcessor
    def frame
    String filename = ""
    boolean done = false

    JtlFileReaderWorker(String inputFile, def cardLayout, def layoutFrame) {
        filename = inputFile
        resultsProcessor = new JMeterResults()

        layout = cardLayout
        frame = layoutFrame
    }

    /**
     * @return   error message when an error occurred, null otherwise
     */
    String doInBackground() {
        resultsProcessor.file = new File(filename)
        try {
            resultsProcessor.parseJMeterResult(frame)
            if (! resultsProcessor.validJtlVersion) {
                return "Not a (valid) .jtl file. \n(JTL version must be 1.2)"
            }
            else if (! resultsProcessor.allResults) {
                return "Empty .jtl file (no samples)"
            }
            done = true
        }
        catch (InterruptedIOException interrupt) {
            // Cancelled by user, do nothing
            return null
        }
        catch (SAXParseException parseError) {
            return "XML parsing error: " + parseError.message
        }
        catch (OutOfMemoryError oom) {
            return "The process ran out of memory. Restart with more memory or select smaller file."
        }
        catch (Throwable e) {
            //e.printStackTrace()
            return "An error occured: ${e}"
        }
    }

    void done() {
        if (done) {
            // Advance wizzard to next step
            layout.next(frame.contentPane)
        }
        else {
            def error = get()
            if (error)
                JOptionPane.showMessageDialog frame, error, "Error", JOptionPane.ERROR_MESSAGE
        }
    }
}
