/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import javax.swing.JPanel
import groovy.swing.SwingBuilder

/**
 * A panel that allows registration of two hook callbacks, that will be called (when used with a compliant layout
 * manager like the BetterCardLayout), when the panel is shown or hidden.
 */
class HooksPanel extends JPanel {
    String id
    SwingBuilder builder
    Closure enterHook
    Closure leaveHook

    void setEnterHook(Closure clos) {
        enterHook = clos
        enterHook.delegate = this
    }

    void setLeaveHook(Closure clos) {
        leaveHook = clos
        leaveHook.delegate = this
    }
}