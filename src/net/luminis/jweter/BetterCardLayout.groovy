/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import java.awt.Component
import java.awt.Container
import java.awt.CardLayout

/**
 * Improved Card-layout manager: will call 'enter' and 'leave' hooks (callback functions) when used with HooksPanel.
 * The callbacks allow to prepare the "tab" to show, or to do some post-processing before leaving a "tab", which
 * might even prevent moving to the next tab (e.g. when input validation fails).
 */
class BetterCardLayout extends CardLayout {

    public void next(Container parent) {
        def current = parent.components.find { it.visible }
        if (current instanceof HooksPanel) {
            if (current.leaveHook) {
                def result = current.leaveHook.call()
                if (result != null && result == false) {
                    // if leave hook explicitly returns false (not null!), cancel the 'next'
                    return
                }
            }
        }
        super.next(parent)
        for (int i = 0; i < parent.getComponentCount(); i++) {
            Component comp = parent.getComponent(i);
            if (comp.isVisible()) {
                if (comp instanceof HooksPanel) {
                    if (comp.enterHook) {
                        comp.enterHook.call()
                    }
                }
            }
        }
    }
}