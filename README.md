# JWeter

![JWeter icon](https://bytebucket.org/pjtr/jweter/raw/master/src/net/luminis/jweter/jweter1.png)

## Overview

JWeter is a tool for analyzing and visualing JMeter result (.jtl) files.

## Usage

Download the jar file and run it with `java -jar JWeter.jar`, or double click the jar file (depends on OS). A wizzard will appear that will guide you through the steps to create a nice graph or error analysis.

## License

JWeter is open source and licensed under LGPL (see the NOTICE.txt and LICENSE.txt files in the distribution). As of the LGPL license, all modifications and additions to the source code must be published as (L)GPL as well.

## Acknowledgements

The beautiful JWeter icon is created by Ivo Domburg.

## Feedback

Of course, all feedback is welcome. Mail the author (peter dot doornbosch) at luminis dot eu, or create an issue at <https://bitbucket.org/pjtr/jweter/issues>.